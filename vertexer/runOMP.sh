#!/bin/bash
iter_per_fixed_th_no=10
function run() {
   rm ../benchmark.out
   touch ../benchmark.out
   for i in {1..8}; do
      export OMP_NUM_THREADS=$i
      echo
      echo "------------ $i thread(s) -------------"
      total_ev1=0
      total_ev2=0
      total_ev3=0 
      for j in $(seq 1 $iter_per_fixed_th_no); do 
         read a b c <<<$(aliroot -b -l -q testVertexer.C+ 2>/dev/null | grep AAA | awk '{print $1="", $0}')
         printf " iter:%2d %3.4f %3.4f %3.4f (ms)\n" $j $a $b $c 
         echo -e "$i\t$a\t$b\t$c" >> ../benchmark.out
         total_ev1=$(echo  "$total_ev1 + $a" | bc)
         total_ev2=$(echo  "$total_ev2 + $b" | bc)
         total_ev3=$(echo  "$total_ev3 + $c" | bc)
         done;
      echo  
      printf " avg:    %3.4f %3.4f %3.4f (ms)\n" $(echo "$total_ev1/$iter_per_fixed_th_no" | bc -l) $(echo "$total_ev2/$iter_per_fixed_th_no" | bc -l) $(echo "$total_ev3/$iter_per_fixed_th_no" | bc -l)
      done;
   }
cd 001
run
cd -
