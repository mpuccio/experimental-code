#!/bin/bash
echo "Patching..."
DIR=$(dirname "$0")
echo $DIR
cp $DIR/*.cxx $ALICE_ROOT/../src/ITS/UPGRADE/ITSUpgradeRec/
cp $DIR/*.h   $ALICE_ROOT/../src/ITS/UPGRADE/ITSUpgradeRec/
cp $DIR/*.txt $ALICE_ROOT/../src/ITS/UPGRADE/ITSUpgradeRec/

cd $ALICE_ROOT/../build && make install

echo "Done!"
